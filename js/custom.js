(function ($, Drupal) {
  Drupal.behaviors.togglemenu = {
    attach: function (context, settings) {
      $(context).find('.topnav li.icon').click(function(e){
        $(context).find('#myTopnav').toggleClass('responsive');
      });
    }
  }
})(jQuery, Drupal);